(load "03.in") ;; provides "dataset"
(load "helpers.scm")

(define map-width (length (string->list (car dataset))))
(define map-height (length dataset))

(define (position-at-step n step-right)
  (if (= n 0)
      0
      (mod (* n step-right) map-width)))

(define (tree? step step-right step-down dataset)
  (let* ([pos (position-at-step step step-right)]
         [ch  (string-ref (list-ref dataset (* step step-down)) pos)])
    (eq? #\# ch)))

(define (trees dataset step-right step-down)
  (map
   (lambda (step) (tree? step step-right step-down dataset))
   (enumerate-interval 0 (ceiling (/ (- map-height 1) step-down)))))

(define (num-trees dataset step-right step-down)
  (length (remq #f (trees dataset step-right step-down))))

(define t1 (current-time))

(display (num-trees dataset 1 1)) (newline)
(display (num-trees dataset 3 1)) (newline)
(display (num-trees dataset 5 1)) (newline)
(display (num-trees dataset 7 1)) (newline)
(display (num-trees dataset 1 2)) (newline)

(display "#1: ") (display (num-trees dataset 3 1)) (newline)
(display "#2: ") (display (* (num-trees dataset 1 1)
                             (num-trees dataset 3 1)
                             (num-trees dataset 5 1)
                             (num-trees dataset 7 1)
                             (num-trees dataset 1 2))) (newline)

(display "Elapsed time: ")
(display (time-difference (current-time) t1))
(newline)
