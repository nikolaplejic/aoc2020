(load "06.in") ;; provides "dataset"
(load "helpers.scm")

(define (documents dataset)
  (let [(current-document '())
        (all-documents '())]

    (define (add-document)
      (set! all-documents (cons current-document all-documents))
      (set! current-document '()))

    (for-each
     (lambda (entry)
       (if (eq? entry "")
           (add-document)
           (set! current-document
                 (cons entry current-document))))
     dataset)

    (add-document)

    all-documents))

(define (count-unique lst)
  (define (count-unique* lst counted)
    (cond [(null? lst) (length counted)]
          [(null? counted) (count-unique* (cdr lst) (cons (car lst) counted))]
          [(equal? (car lst) (car counted)) (count-unique* (cdr lst) counted)]
          [else (count-unique* (cdr lst) (cons (car lst) counted))]))

  (count-unique* (sort char-ci<? lst) '()))

;; --

(define (number-of-positives document)
  (count-unique (flatmap string->list document)))

(define (positives-in-groups groups)
  (map number-of-positives groups))

(define (sum-of-positives groups)
  (let ([positives (positives-in-groups groups)])
    (accumulate + 0 positives)))

;; --

;;;; For a given group, keep a hashtable where the keys are characters
;;;; representing an answer, and values are number of occurences of the
;;;; character throughout the group.
;;;;
;;;; Number of unique positives in a group is the number of entries in the
;;;; hashtable which have the number of occurences equal to the number of
;;;; entries.

(define (unique-positives group)
  (let ([ht (make-eq-hashtable)]
        [members (length group)])

    (define (count-occurences* answers)
      (map
       (lambda (a) (hashtable-update! ht a (lambda (v) (+ 1 v)) 0))
       (string->list answers)))

    (map count-occurences* group)

    (let* ([vvals (hashtable-values ht)]
           [vals (vector->list vvals)])
      (length (filter (lambda (n) (= members n)) vals)))))

(define (unique-positives-in-groups groups)
  (map unique-positives groups))

(define (sum-of-unique-positives groups)
  (let ([positives (unique-positives-in-groups groups)])
    (accumulate + 0 positives)))

;; --

(define t1 (current-time))

(display "#1: ") (display (sum-of-positives (documents dataset))) (newline)
(display "#2: ") (display (sum-of-unique-positives (documents dataset))) (newline)

(display "Elapsed time: ")
(display (time-difference (current-time) t1))
(newline)

