(load "04.in") ;; provides "dataset"
(load "helpers.scm")

(define required-fields '("ecl" "pid" "eyr" "hcl" "byr" "iyr" "hgt"))

;; --

(define (documents dataset)
  (let [(current-document '())
        (all-documents '())]

    (define (add-document)
      (set! all-documents (cons current-document all-documents))
      (set! current-document '()))

    (for-each
     (lambda (entry)
       (if (eq? entry "")
           (add-document)
           (set! current-document
                 (cons (string-append " " entry) current-document))))
     dataset)

    (add-document)

    (map
     (lambda (seq) (accumulate string-append "" seq))
     all-documents)))

(define (fields document)
  (let* [(spaces (char-range document #\space))
         (range (append
                 spaces
                 (list (string-length document))))]
    (map (lambda (i)
           (let [(start (+ 1 (list-ref range i)))
                 (end (list-ref range (+ 1 i)))]
             (substring document start end)))
         (enumerate-interval 0 (- (length range) 2)))))

(define (field-name field)
  (let [(colon (car (char-range field #\:)))]
    (substring field 0 colon)))

(define (field-value field)
  (let [(colon (car (char-range field #\:)))]
    (substring field (+ 1 colon) (string-length field))))

(define (field-names document)
  (let [(fields (fields document))]
    (map field-name fields)))

;; --

(define (contains-required-fields? document)
  (let* [(field-names   (field-names document))
         (has-required? (map
                         (lambda (required-field)
                           (list-contains? field-names required-field))
                         required-fields))]
    (= 0 (length (remq #t has-required?)))))

;; --

(define (validate-byr v)
  (let [(n (string->number v))]
    (and n (and (>= n 1920) (<= n 2002)))))

(define (validate-iyr v)
  (let [(n (string->number v))]
    (and n (and (>= n 2010) (<= n 2020)))))

(define (validate-eyr v)
  (let [(n (string->number v))]
    (and n (and (>= n 2020) (<= n 2030)))))

(define (validate-ecl v)
  (let [(valid-options '("amb" "blu" "brn" "gry" "grn" "hzl" "oth"))]
    (list-contains? valid-options v)))

(define (validate-pid v)
  (and (string->number v) (= 9 (string-length v))))

(define (validate-hgt v)
  (define (validate* v len)
    (let* [(unit (substring v (- len 2) len))
           (height* (substring v 0 (- len 2)))
           (height (string->number height*))]
      (cond [(not height) #f]
            [(equal? "cm" unit) (and (>= height 150) (<= height 193))]
            [(equal? "in" unit) (and (>= height 59) (<= height 76))]
            [else #f])))

  (let* [(len (string-length v))]
    (if (> len 3)
        (validate* v len)
        #f)))

(define (validate-hcl v)
  (define allowed-chars
    '(#\a #\b #\c #\d #\e #\f #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9))

  (define (validate* v)
    (and (equal? "#" (substring v 0 1))
         (= 0 (length
               (remq #t
                     (map
                      (lambda (c) (list-contains? allowed-chars c))
                      (string->list (substring v 1 (string-length v)))))))))

  (let* [(len (string-length v))]
    (if (= len 7)
        (validate* v)
        #f)))

(define (valid-value? field)
  (let [(field-name (field-name field))
        (field-value (field-value field))]
    (cond [(equal? field-name "byr") (validate-byr field-value)]
          [(equal? field-name "iyr") (validate-iyr field-value)]
          [(equal? field-name "eyr") (validate-eyr field-value)]
          [(equal? field-name "hgt") (validate-hgt field-value)]
          [(equal? field-name "hcl") (validate-hcl field-value)]
          [(equal? field-name "ecl") (validate-ecl field-value)]
          [(equal? field-name "pid") (validate-pid field-value)]
          [(equal? field-name "cid") #t]
          [else #f])))

(define (has-valid-values? document)
  (let [(fields (fields document))]
    (= 0 (length (remq #t (map valid-value? fields))))))

;; --

(define t1 (current-time))

(define num-valid-documents1
  (length (remq #f (map contains-required-fields? (documents dataset)))))

(define num-valid-documents2
  (length
   (remq #f
         (map
          (lambda (doc)
            (and (contains-required-fields? doc) (has-valid-values? doc)))
          (documents dataset)))))

(display "#1: ") (display num-valid-documents1) (newline)
(display "#2: ") (display num-valid-documents2) (newline)

(display "Elapsed time: ")
(display (time-difference (current-time) t1))
(newline)
