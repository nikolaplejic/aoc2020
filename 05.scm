(load "05.in") ;; provides "dataset"
(load "helpers.scm")

(define (rows entry) (string->list (substring entry 0 7)))
(define (cols entry) (string->list (substring entry 7 10)))

(define (calculate-place s min max lower-char)
  (cond [(= 0 (length s)) (error "guru meditation.")]
        [(= 1 (length s)) (if (equal? lower-char (car s))
                                (inexact->exact min) (inexact->exact max))]
        [else (let [(half (floor (/ (+ min max) 2.0)))]
                (if (equal? lower-char (car s))
                    (calculate-place (cdr s) min half lower-char)
                    (calculate-place (cdr s) (+ 1 half) max lower-char)))]))

(define (calculate-row row) (calculate-place row 0 127 #\F))
(define (calculate-col col) (calculate-place col 0 7 #\L))
(define (seat-id seat)
  (let ([row (calculate-row (rows seat))]
        [col (calculate-col (cols seat))])
    (+ (* 8 row) col)))

(define (my-seat dataset)
  (define nonsequential-pair
    (let* ([seats (sort > (map seat-id dataset))]
           [seat-pairs (map (lambda (x y) (cons x y)) seats (append (cdr seats) (list 0)))])
      (car (filter
            (lambda (pair) (and (not (= 1 (- (car pair) (cdr pair))))
                                (not (= 0 (cdr pair)))))
            seat-pairs))))
  (/ (+ (car nonsequential-pair) (cdr nonsequential-pair)) 2))


(define t1 (current-time))

(display "#1: ") (display (car (sort > (map seat-id dataset)))) (newline)
(display "#2: ") (display (my-seat dataset)) (newline)

(display "Elapsed time: ")
(display (time-difference (current-time) t1))
(newline)
