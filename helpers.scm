(define (accumulate op initial sequence)
	(if (null? sequence)
	    initial
	    (op (car sequence)
	        (accumulate op initial (cdr sequence)))))

(define (flatmap proc seq)
	(accumulate append '() (map proc seq)))

(define (enumerate-interval i j)
  (if (> i j)
      '()
      (cons i (enumerate-interval (+ i 1) j))))

(define (identity z) z)

(define (char-range entry char)
  (let ([positions (map
                    (lambda (i) (if (eq? char (string-ref entry i)) i #f))
                    (enumerate-interval 0 (- (string-length entry) 1)))])
    (remq #f positions)))

(define (list-contains? lst element)
  (cond [(null? lst) #f]
        [(equal? (car lst) element) #t]
        [else (list-contains? (cdr lst) element)]))
