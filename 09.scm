(load "09.in") ;; provides "dataset"
(load "helpers.scm")

(define datasetv (list->vector dataset))

(define (at-position position)
  (vector-ref datasetv position))

(define (previous-range position n)
  (define (previous-range i result)
    (if (>= i position)
        result
        (previous-range (+ 1 i) (cons (vector-ref datasetv i) result))))

  (previous-range (- position n) '()))

(define (combinations l)
  (define result '())

  (for-each
   (lambda (x)
     (for-each
      (lambda (y)
        (when (not (= x y))
          (set! result (cons (cons x y) result))))
      l))
   l)

  result)

(define (is-sum-of-previous-n? position n)
  (let* ([number (at-position position)]
         [prev   (previous-range position n)]
         [combs  (combinations prev)]
         [sums   (map (lambda (t) (+ (car t) (cdr t))) combs)]
         [is-sum (> (length (filter (lambda (t) (= t number)) sums)) 0)])
    (cons is-sum (cons position number))))

(define (all-sums n)
  (map
   (lambda (i) (is-sum-of-previous-n? i n))
   (enumerate-interval (+ 1 n) (- (vector-length datasetv) 1))))

(define (contiguous-sum number dataset)
  (define (contiguous-sum-1 number dataset r)
    (cond [(null? dataset) #f]
          [(equal? number (apply + r)) r]
          [(> (apply + r) number) #f]
          [else (contiguous-sum-1 number (cdr dataset) (cons (car dataset) r))]))

  (define (contiguous-sum-2 number dataset)
    (let ([sum (contiguous-sum-1 number dataset '())])
      (if (not (equal? #f sum))
          sum
          (contiguous-sum-2 number (cdr dataset)))))

  (contiguous-sum-2 number dataset))

(define t1 (current-time))

(let* ([sol1 (cddar (filter (lambda (t) (not (car t))) (all-sums 25)))]
       [sum  (sort < (contiguous-sum sol1 dataset))]
       [suml (length sum)]
       [sol2 (+ (car sum) (list-ref sum (- suml 1)))])
  (display "#1: ") (display sol1) (newline)
  (display "#2: ") (display sol2) (newline))

(display "Elapsed time: ")
(display (time-difference (current-time) t1))
(newline)
