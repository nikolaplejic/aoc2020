(load "07.in") ;; provides "dataset"
(load "helpers.scm")

(define (parse-contains rule-string)
  (define (extract-rule-strings rule-string)
    (let* ([boundaries (cons 0 (append
                                (char-range rule-string #\,)
                                (list (string-length rule-string))))]
           [idxs (enumerate-interval 0 (- (length boundaries) 2))])
      (map
       (lambda (i)
         (let (;; adding 2 avoids the ", " in the front
               [start (if (= 0 i) 0 (+ 2 (list-ref boundaries i)))]
               ;; subtracting 5 eliminates the " bags" suffix
               [end (list-ref boundaries (+ 1 i))])
           (substring rule-string start end)))
       idxs)))

  (define (parse-rule-strings rule-string)
    (let* ([boundaries (char-range rule-string #\space)]
           [nr (string->number (substring rule-string 0 (list-ref boundaries 0)))]
           [start (+ 1 (list-ref boundaries 0))]
           [end (if (= 3 (length boundaries)) (list-ref boundaries 2) (string-length rule-string))]
           [color (substring rule-string start end)])
      (if (equal? "other" color)
          #f
          (cons nr color))))

  (map parse-rule-strings (extract-rule-strings rule-string)))

(define (parse-rule rule-string)
  (let* ([spaces (char-range rule-string #\space)]
         [color-boundary (list-ref spaces 1)]
         [main-color (substring rule-string 0 color-boundary)]
         [contains-boundary (cons (+ 1 (list-ref spaces 3))
                                  (- (string-length rule-string) 1))]
         [contains-rule (substring rule-string
                                   (car contains-boundary)
                                   (cdr contains-boundary))]
         [contains (parse-contains contains-rule)])
    (cons main-color contains)))

(define (get-container-color rule) (car rule))
(define (get-contained-colors rule)
  (if (equal? #f (cadr rule))
      '()
      (map (lambda (c) (cdr c)) (cdr rule))))

(define (rules dataset) (map parse-rule dataset))

(define (is-container-for? color rule)
  (list-contains? (get-contained-colors rule) color))

(define (containers-for color rules)
  (map get-container-color
       (filter (lambda (t) (is-container-for? color t)) rules)))

(define (contains color rules)
  (let ([rule (car (filter (lambda (rule) (equal? color (get-container-color rule))) rules))])
    (cdr rule)))

(define (combo-contains color rules)
  (define result '())

  (define (combo-contains* c)
    (let ([containers (containers-for c rules)])
      (if (> (length containers) 0)
          (for-each
           (lambda (c)
             (unless (list-contains? result c) (set! result (cons c result)))
             (combo-contains* c))
           containers)
          (unless (list-contains? result c) (set! result (cons c result))))))

  (combo-contains* color)
  (length result))

(define (treeize color rules)
  (let ([items (contains color rules)])
    (if (equal? (caar items) #f)
        '()
        (map
         (lambda (e)
           (cons (car e) (treeize (cdr e) rules)))
         items))))

(define (combo-contained t)
  (cond [(and (number? (car t)) (null? (cdr t))) (car t)]
        [(number? (car t)) (+ (car t) (* (car t) (apply + (map combo-contained (cdr t)))))]
        [else (apply + (map combo-contained t))]))

(define t1 (current-time))

(define dsrules (rules dataset))
(display "#1: ") (display (combo-contains "shiny gold" dsrules)) (newline)
(display "#2: ") (display (combo-contained (treeize "shiny gold" dsrules))) (newline)

(display "Elapsed time: ")
(display (time-difference (current-time) t1))
(newline)
