(load "01.in") ;; provides "dataset"
(load "helpers.scm")

(define (slv1 dataset)
  (let ([result (filter
                 (lambda (resultmap) (= 2020 (caddr resultmap)))
                 (map
                  (lambda (x) (list x (car dataset) (+ x (car dataset))))
                  (cdr dataset)))])
    (if (null? result)
        (slv1 (cdr dataset))
        (map (lambda (x) (* (car x) (cadr x))) result))))

(define (slv2 dataset)
  (let ([results (flatmap
                  (lambda (e1)
                    (flatmap
                     (lambda (e2)
                       (filter
                        (lambda (x) (= (car x) 2020))
                        (map
                         (lambda (e3) (cons (+ e1 e2 e3) (list e1 e2 e3)))
                         (cddr dataset))))
                     (cdr dataset)))
                  dataset)])
    (map (lambda (x) (* (cadr x) (caddr x) (cadddr x))) results)))

(display "#1: ") (display (car (slv1 dataset))) (newline)
(display "#2: ") (display (car (slv2 dataset))) (newline)
