# Advent Of Code 2020

These are my solutions. There are many like them, but these ones are mine.

R6RS Scheme. Tested in Chez. Your Scheme implementation may vary.

Portions of code in `helpers.scm` shamelessly summoned from "Structure and
Interpretation of Computer Programs".
