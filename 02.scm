(load "02.in") ;; provides "dataset"
(load "helpers.scm")

;; --

(define (rule-string entry)
  (let ([max (car (char-range entry #\space))])
    (substring entry 0 max)))

(define (rule entry)
  (let ([hyphen (car (char-range (rule-string entry) #\-))])
    (cons
     (string->number (substring entry 0 hyphen))
     (string->number (substring entry (+ 1 hyphen) (string-length (rule-string entry)))))))

(define (letter entry)
  (let ([letter-range (char-range entry #\space)])
    (string-ref (substring entry (+ 1 (car letter-range)) (- (cadr letter-range) 1)) 0)))

(define (password entry)
  (let ([password-start (cadr (char-range entry #\space))])
    (substring entry (+ 1 password-start) (string-length entry))))

;; --

(define (valid1? entry)
  (let ([rule (rule entry)]
        [num-chars (length (char-range (password entry) (letter entry)))])
    (and (>= num-chars (car rule))
         (<= num-chars (cdr rule)))))

(define (valid2? entry)
  (let ([pos1 (- (car (rule entry)) 1)]
        [pos2 (- (cdr (rule entry)) 1)]
        [letter (letter entry)]
        [password (password entry)])
    (or (and (eq? letter (string-ref password pos1))
             (not (eq? letter (string-ref password pos2))))
        (and (not (eq? letter (string-ref password pos1)))
             (eq? letter (string-ref password pos2))))))


(define (num-valid rule dataset)
  (length (remq #f (map rule dataset))))

(display "#1: ") (display (num-valid valid1? dataset)) (newline)
(display "#2: ") (display (num-valid valid2? dataset)) (newline)
