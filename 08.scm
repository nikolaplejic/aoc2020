(load "08.in") ;; provides "dataset"
(load "helpers.scm")

(define (parse-instruction instr)
  (let ([space (car (char-range instr #\space))]
        [len (string-length instr)])
    (cons (substring instr 0 space)
          (string->number (substring instr (+ 1 space) len)))))

(define (instructions dataset)
  (list->vector (map parse-instruction dataset)))

(define (op i) (car i))
(define (arg i) (cdr i))

(define (exec instructions)
  (define (inc-exec-count! position)
    (vector-set! execs position (+ 1 (vector-ref execs position))))

  (define (exec* position)
    (if (>= position (vector-length execs))
        ;; the program has tried to execute beyond its last instruction, and
        ;; thus has terminated
        (cons "term" acc)
        (let* ([ins (vector-ref instructions position)]
               [op (op ins)]
               [arg (arg ins)]
               [exec-count (vector-ref execs position)])
          (inc-exec-count! position)
          (cond [(= 1 exec-count) (cons "inf loop" acc)]
                [(equal? "nop" op) (exec* (+ 1 position))]
                [(equal? "jmp" op) (exec* (+ arg position))]
                [else
                 (set! acc (+ arg acc))
                 (exec* (+ 1 position))]))))

  (set! acc 0)
  (set! execs (make-vector (length dataset) 0))

  (exec* 0))

(define (find-inss instructions ins)
  (filter
   (lambda (i) (equal? ins (op (list-ref instructions i))))
   (enumerate-interval 0 (- (length instructions) 1))))

(define (modify-jmps instructions)
  (define (modify-jmp* jmp)
    (let ([new-vector (vector-copy instructions)]
          [i (vector-ref instructions jmp)])
      (vector-set! new-vector jmp (cons "nop" (arg i)))
      new-vector))

  (let ([jmps (find-inss (vector->list instructions) "jmp")])
    (map modify-jmp* jmps)))

(define (modify-nops instructions)
  (define (modify-nop* nop)
    (let ([new-vector (vector-copy instructions)]
          [i (vector-ref instructions nop)])
      (vector-set! new-vector nop (cons "jmp" (arg i)))
      new-vector))

  (let ([nops (find-inss (vector->list instructions) "nop")])
    (map modify-nop* nops)))

;; --

(define t1 (current-time))

(define instructions (instructions dataset))

(display "#1: ") (display (cdr (exec instructions))) (newline)
(display "#2a: ")
(display
 (filter
  (lambda (exec) (equal? "term" (car exec)))
  (map exec (modify-jmps instructions))))
(newline)
(display "#2b: ")
(display
 (filter
  (lambda (exec) (equal? "term" (car exec)))
  (map exec (modify-nops instructions))))
(newline)

(display "Elapsed time: ")
(display (time-difference (current-time) t1))
(newline)
